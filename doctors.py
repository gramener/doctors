"""
Scrapes the list of doctors from the MCI site.

Usage:
    python doctors.py 1
    python doctors.py 2
    python doctors.py 3
    ...
    python doctors.py 940
    python doctors.py 941

... or better yet (to run 8 processes in parallel)

    seq 1 941 | xargs -n 1 -P 8 python doctors.py

This creates a series of mci-xxxx.csv files, each containing every doctor
whose ID begins with the command line parameter specified.

"""

import sys
import csv
import urllib
import lxml.html

fields = [
    'ID',
    'Name',
    'Registration No.',
    'Year of Info',
    'State Medical Council',
    'Date of Birth',
    'Date of Reg.',
    'Father/Husband Name',
    'Qualification',
    'Qualification Year',
    'University Name',
    'Permanent Address',
    ]

def get_url(url):
    return urllib.urlopen(url).read()

def parse(id):
    url = 'http://www.mciindia.org/ViewDetails.aspx?ID=%d' % id
    tree = lxml.html.fromstring(get_url(url))
    result = { 'ID': id }
    for row in tree.cssselect('tr'):
        tds = row.cssselect('td')
        if len(tds) < 2:
            continue
        key   = tds[0].text_content().strip()
        value = tds[1].text_content().strip()
        result[key] = value
    return [result.get(field, '') for field in fields]

block = int(sys.argv[1])
out = csv.writer(open('mci-%04d.csv' % block, 'w'), lineterminator='\n')
out.writerow(fields)
for num in range(0, 1000):
    id = block*1000 + num
    print id
    out.writerow(parse(id))
